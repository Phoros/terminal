package main

type CliFlags struct {
}

type Environment struct {
	Docker struct {
		Path    string
		Version string
	}
}

type Bootstrap struct {
	Networks struct {
		Name string			`json:"name"`
		IPv4CIDR string		`json:"ipv4CIDR"`
		IPv6CIDR string		`json:"ipv6CIDR"`
		IsAttachable bool	`json:"isAttachable"`
		IsIsolated bool		`json:"isIsolated"`
	}
}

type Network struct {
	Name string
	IPAddress string
}

type Mount struct {
	Name string
	Type string
	Source string
	Destination string
	CopyIntoVolume bool
}

type Project struct {
	Path string
	Data struct {
		General struct {
			Name string
			Version string
		}
		Docker struct {
			RestartPolicy string
			Hostname string
			Mounts []Mount
			Networks []Network
			PrivatePorts []int
			PublicPorts []string
			AdditionalOptions string
			Dependencies []string
		}
		Host struct {
			DataDirectory string
			Copy []string
			Execute struct {
				Env []string
				Commands []string
			}
		}
	}
}
