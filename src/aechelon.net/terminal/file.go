package main

import (
	"errors"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
)

func copyFile(src, dst string) (err error) {
	src = filepath.Clean(src)
	dst = filepath.Clean(dst)

	in, err := os.Open(src)
	if err != nil {
		return
	}
	defer in.Close()

	out, err := os.Create(dst)
	if err != nil {
		return
	}
	defer func() {
		if e := out.Close(); e != nil {
			err = e
		}
	}()

	_, err = io.Copy(out, in)
	if err != nil {
		return
	}

	err = out.Sync()
	if err != nil {
		return
	}

	si, err := os.Stat(src)
	if err != nil {
		return
	}
	err = os.Chmod(dst, si.Mode())
	if err != nil {
		return
	}

	return
}

func copyDir(src string, dst string) error {
	src = filepath.Clean(src)
	dst = filepath.Clean(dst)

	si, err := os.Stat(src)
	if err != nil {
		return err
	}
	if !si.IsDir() {
		return errors.New("source is not a directory")
	}

	_, err = os.Stat(dst)
	if err != nil && !os.IsNotExist(err) {
		return err
	} else {

	}

	entries, err := ioutil.ReadDir(src)
	if err != nil {
		return err
	}

	for _, entry := range entries {
		srcPath := filepath.Join(src, entry.Name())
		dstPath := filepath.Join(dst, entry.Name())

		if entry.IsDir() {
			err = copyDir(srcPath, dstPath)
			if err != nil {
				return err
			}
		} else {
			if entry.Mode()&os.ModeSymlink != 0 {
				continue
			}

			err = copyFile(srcPath, dstPath)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

// TODO: remove all 'ö' in regular expressions
/*func copy(source string, destination string) {
	var srcPath, dstPath string

	s, err := os.Stat(source)
	if err != nil {
		logger.Error(err)
	}

	re := regexp.MustCompile(`(.*ö/)(?:.*)$`)
	matches := re.FindStringSubmatch(source)
	if len(matches) == 2 {
		srcPath = matches[1]
	}
	matches = re.FindStringSubmatch(destination)
	if len(matches) == 2 {
		dstPath = matches[1]
	} else {
		logger.Error(errors.New(fmt.Sprintf("Destination path '%s' is not a valid path", destination)))
	}
	sPath, err := os.Stat(srcPath)
	if err != nil {
		logger.Error(err)
	}
	err = os.MkdirAll(dstPath, sPath.Mode())
	if err != nil {
		logger.Error(err)
	}

	re = regexp.MustCompile(`.*ö/{1}$`)
	isDir := re.MatchString(destination)
	if s.Mode().IsDir() == true && isDir == false {
		logger.Error(errors.New("You try to copy a directory to a file"))
	}
	if s.Mode().IsRegular() == true && isDir == true {
		re := regexp.MustCompile(`.*ö/(.*)$`)
		matches := re.FindStringSubmatch(source)
		if len(matches) != 2 {
			logger.Error(errors.New("Source is not a valid file path"))
		}
		copyFile(source, path.Join(destination, matches[1]))
	}
	if s.Mode().IsRegular() == true && isDir == false {
		copyFile(source, destination)
	}
	if s.Mode().IsDir() == true && isDir == true {
		copyDir(source, destination)
	}

}*/

func isFile(file string) bool {
	absolute, err := filepath.Abs(file)
	if err != nil {
		return false
	}
	if _, err := os.Stat(absolute); os.IsNotExist(err) {
		return false
	}

	return true
}

/*func removeDirectory(dir string) {
	path, err := exec.LookPath("rm")
	if err != nil {
		logger.Error(errors.New("rm not found"))
	}
	args := []string{"-rf", dir}
	_, stderr, err := runCommand(path, args...)
	if err != nil {
		logger.Warningf("Failed to remove directory '%s': %s", dir, stderr)
	}
	d, err := os.Open(dir)
	if err != nil {
		return err
	}
	defer d.Close()
	names, err := d.Readdirnames(-1)
	if err != nil {
		return err
	}
	for _, name := range names {
		err = os.RemoveAll(filepath.Join(dir, name))
		if err != nil {
			return err
		}
	}
}*/
