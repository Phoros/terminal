package main

import (
	"aechelon.net/logplus"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/mattn/go-shellwords"
	"gopkg.in/yaml.v2"
)

var (
	NamePattern = `^(?:[a-z][a-z0-9]*\/)?([a-z][a-z0-9-_]*)$`
)

func getContainerName(name string) string {
	re := regexp.MustCompile(NamePattern)
	matches := re.FindStringSubmatch(name)
	return matches[1]
}

func parseProject(path string) Project {
	absolutePath, _ := filepath.Abs(path)

	project := Project{}
	project.Path = absolutePath

	logplus.Print("= Parse project(s)")
	if _, err := os.Stat(filepath.Join(project.Path + "/.Terminal")); os.IsNotExist(err) {
		logplus.Error(err.Error())
	}
	data, err := ioutil.ReadFile(filepath.Join(project.Path + "/.Terminal"))
	if err != nil {
		logplus.Error(err.Error())
	}
	err = yaml.Unmarshal([]byte(data), &project.Data)
	if err != nil {
		logplus.Error(err.Error())
	}

	if project.Data.General.Name == "" {
		logplus.Error(("Project name is empty")
	}
	re := regexp.MustCompile(NamePattern)
	validName := re.MatchString(project.Data.General.Name)
	if validName == false {
		logplus.Error("Project name contains invalid characters")
	}
	logplus.Printfln("Name: %v", project.Data.General.Name)

	if project.Data.General.Version == "" {
		logplus.Error("Project version is empty")
	}
	logplus.Printfln("Version: %v", project.Data.General.Version)

	if project.Data.Docker.Hostname == "" {
		logplus.Warning("Project hostname is empty")
	} else {
		logplus.Printfln("Hostname: %s", project.Data.General.Name)
	}

	if project.Data.Docker.RestartPolicy == "" {
		logger.Println("No restart policy defined")
	} else {
		logplus.Printfln("Restart policy: %v", project.Data.General.Name)
	}

	if len(project.Data.Docker.Mounts) > 0 {
		logplus.Printfln("Mounts: %s", project.Data.Docker.Mounts)
	}

	if len(project.Data.Docker.Networks) > 0 {
		logplus.Printfln("Networks: %s", project.Data.Docker.Networks)
	}

	if len(project.Data.Docker.PrivatePorts) > 0 {
		logplus.Printfln("Private ports: %v", project.Data.Docker.PrivatePorts)
	}

	if len(project.Data.Docker.PublicPorts) > 0 {
		logplus.Printfln("Public ports: %s", project.Data.Docker.PublicPorts)
	}

	if project.Data.Docker.AdditionalOptions != "" {
		logplus.Printfln("Additional options: %s", project.Data.Docker.AdditionalOptions)
	}

	if len(project.Data.Host.DataDirectory) > 0 {
		containerName := getContainerName(project.Data.General.Name)
		r := strings.NewReplacer("%CONTAINER%", containerName)
		project.Data.Host.DataDirectory = r.Replace(project.Data.Host.DataDirectory)
		logplus.Printfln("Host data directory: %s", project.Data.Host.DataDirectory)
	} else {
		if len(project.Data.Docker.Mounts) > 0 {
			logplus.Error("You cannot have any mounts without a data directory")logplus.
		}
	}

	if len(project.Data.Host.Execute.Commands) > 0 {
		if len(project.Data.Host.Execute.Env) > 0 {
			additionalEnv := []string{fmt.Sprintf("PROJECT_PATH=%s", project.Path)}
			project.Data.Host.Execute.Env = append(project.Data.Host.Execute.Env, additionalEnv...)
			logplus.Printfln("Command environment variables: %v", project.Data.Host.Execute.Env)
		}
		logplus.Printfln("Command executions: %v", project.Data.Host.Execute.Commands)

	}

	return project
}

func buildSingleProject(path string) {
	logger.Println("=> Parse project")
	project := parseProject(path)
	containerName := getContainerName(project.Data.General.Name)

	logger.Println("\n=> Check if container exists")
	if checkIfContainerExists(containerName) {
		if flags.kill {
			killDockerContainer(containerName)
		} else {
			logger.Errorf("Container '%s' already exists. Please remove it.", containerName)
		}

	} else {
		logger.Printfln("Container '%s' does not exist", containerName)
	}
	if flags.purge {
		if project.Data.Host.DataDirectory != "" {
			logger.Println("\n=> Clean project data directory")
			cleanDockerMounts(project.Data.Host.DataDirectory)
		}
	}
	if flags.initialize == true {
		if len(project.Data.Docker.Networks) > 0 {
			logger.Println("\n=> Create Docker networks")
			createDockerNetworks(project.Data.Docker.Networks)
		}
	}
	if len(project.Data.Host.Execute.Commands) > 0 {
		logger.Println("\n=> Execute commands")
		executeCommands(project)
	}
	if flags.tokenize == true {
		logger.Println("\n=> Tokenize projects")
		tokenizeProject(project)
	}
	if len(project.Data.Host.Copy) > 0 {
		logger.Println("\n=> Copy project files")
		copyProjectFiles(project)
	}

	if !flags.nobuild {
		logger.Println("\n=> Create Docker images")
		if _, err := os.Stat(filepath.Join(project.Path, "Dockerfile")); os.IsNotExist(err) {
			logger.Warning("No Dockerfile found")
		} else {
			buildDockerImage(project.Path, project.Data.General.Name, project.Data.General.Version)
		}
	}

	if flags.log || flags.start {
		logger.Println("\n=> Start Docker container")
		startContainer(project)
	}

	if flags.log {
		logger.Println("\n=> Print Docker log output")
		printDockerLog(containerName)
	}
}

func buildMultipleProjects(projects []string) {
	var parsedProjects []Project
	var checkCopy bool

	logger.Println("=> Parse found project paths")
	for _, project := range projects {
		pp := parseProject(project)
		parsedProjects = append(parsedProjects, pp)
		if len(pp.Data.Host.Copy) > 0 {
			checkCopy = true
		}
	}

	logger.Println("\n=> Check for existing containers")
	for _, project := range parsedProjects {
		containerName := getContainerName(project.Data.General.Name)
		if checkIfContainerExists(containerName) {
			if flags.kill || flags.purge {
				killDockerContainer(containerName)
			} else {
				logger.Errorf("Container '%s' already exists. Please remove it.", containerName)
			}

		} else {
			logger.Printfln("Container '%s' does not exist", containerName)
		}
	}

	if flags.initialize == true {
		logger.Println("\n=> Create Docker networks")
		for _, project := range parsedProjects {
			if len(project.Data.Docker.Networks) > 0 {
				createDockerNetworks(project.Data.Docker.Networks)
			}
		}

		if flags.purge {
			logger.Println("\n=> Clean project data directories")
			for _, project := range parsedProjects {
				if project.Data.Host.DataDirectory != "" {
					cleanDockerMounts(project.Data.Host.DataDirectory)
				}
			}
		}

	}

	logger.Println("\n=> Execute commands")
	for _, project := range parsedProjects {
		logger.Printfln("--- Project %s ---", project.Data.General.Name)
		if len(project.Data.Host.Execute.Commands) > 0 {
			executeCommands(project)
		}
	}

	if flags.tokenize == true {
		logger.Println("\n=> Tokenize projects")
		for _, project := range parsedProjects {
			tokenizeProject(project)
		}
	}

	if checkCopy {
		logger.Println("\n=> Copy project files")
		for _, project := range parsedProjects {
			if len(project.Data.Host.Copy) > 0 {
				logger.Printfln("%s", project.Data.General.Name)
				copyProjectFiles(project)
			}
		}
	}

	if !flags.nobuild {
		logger.Println("\n=> Create docker images")
		for _, project := range parsedProjects {
			logger.Println(filepath.Join(project.Path, "Dockerfile"))
			if _, err := os.Stat(filepath.Join(project.Path, "Dockerfile")); os.IsNotExist(err) {
				logger.Warningf("Project '%s' does not have a Dockerfile", project.Data.General.Name)
			} else {
				buildDockerImage(project.Path, project.Data.General.Name, project.Data.General.Version)
			}
		}
	}
	// TODO: Add dependency chain to .Terminal file and start them in an ordered fashion
}

func copyProjectFiles(project Project) {
	for _, element := range project.Data.Host.Copy {
		re := regexp.MustCompile(`((?:\/|.|%|")[a-zA-Z0-9[:blank:]\/.%]+"?)[[:blank:]]{1}((?:\/|.|%|")[a-zA-Z0-9[:blank:]\/.%]+)`)
		if re.MatchString(element) {
			paths := re.FindStringSubmatch(element)
			if len(paths) == 3 {
				re = regexp.MustCompile(`^.\/`)
				r := strings.NewReplacer("%DATA%", project.Data.Host.DataDirectory)
				source := strings.Trim(r.Replace(paths[1]), `" `)
				destination := strings.Trim(r.Replace(paths[2]), `" `)
				source = re.ReplaceAllString(source, project.Path+"/")
				destination = re.ReplaceAllString(destination, project.Path+"/")
				logger.Printfln("Copy %s to %s ", source, destination)
				copy(source, destination)
			} else {
				logger.Warningf("Invalid 'Copy' line in project config: %s", element)
			}

		} else {
			logger.Warningf("Invalid 'Copy' line in project config: %s", element)
		}
	}
}

func executeCommands(project Project) {
	for _, element := range project.Data.Host.Execute.Commands {
		var err error

		containerName := getContainerName(project.Data.General.Name)
		r := strings.NewReplacer("%CONTAINER%", containerName, "%PROJECT%", project.Path)
		element = r.Replace(element)

		p := shellwords.NewParser()
		args, err := p.Parse(element)
		if err != nil {
			logger.Error(errors.New(fmt.Sprintf("Cannot parse line: %v", element)))
		}
		privileges := args[0]
		args = args[1:]
		re := regexp.MustCompile("^([a-z]+)??:?([a-z]+)$")
		matches := re.FindStringSubmatch(privileges)
		if len(matches) == 0 {
			logger.Error(errors.New(fmt.Sprintf("User / Group not valid: %v", privileges)))
		}
		if matches[1] == "" {
			logger.Printfln("Executing '%v' as user '%s'", args, matches[2])
			err = runCommandAsUserWithEnv(project.Data.Host.Execute.Env, matches[2], "", args[0], args[1:]...)
		} else {
			logger.Printfln("Executing '%v' as user '%s' and group '%s'", args, matches[1], matches[2])
			err = runCommandAsUserWithEnv(project.Data.Host.Execute.Env, matches[1], matches[2], args[0], args[1:]...)
		}
		if err != nil {
			logger.Error(err)
		}
	}
}
