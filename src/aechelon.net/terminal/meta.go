package main

import (
	"aechelon.net/logplus"
	"bufio"
	"bytes"
	"os"
	"os/exec"
	"os/user"
	"strconv"
	"strings"
	"syscall"
)

func askForConfirmation(message string) bool {
	for {
		reader := bufio.NewReader(os.Stdin)
		logger.Printf(message)
		input, _ := reader.ReadString('\n')
		if strings.TrimRight(input, "\n") == "yes" {
			return true
		} else if strings.TrimRight(input, "\n") == "no" {
			return false
		}
	}
}

func checkIfRoot() {
	if os.Geteuid() != 0 {
		logplus.Errorf("You need root permissions to start %s", name)
	}
}

func runCommand(command string, arg ...string) int {
	var exitcode int

	cmd := exec.Command(command, arg...)
	err := cmd.Run()

	if err != nil {
		if exiterror, ok := err.(*exec.ExitError); ok {
			ws := exiterror.Sys().(syscall.WaitStatus)
			exitcode = ws.ExitStatus()
		} else {
			exitcode = 255
		}
	} else {
		ws := cmd.ProcessState.Sys().(syscall.WaitStatus)
		exitcode = ws.ExitStatus()
	}

	return exitcode
}

func runCommandWithOutput(command string, arg ...string) (bytes.Buffer, bytes.Buffer, int) {
	var stdout, stderr bytes.Buffer
	var exitcode int

	cmd := exec.Command(command, arg...)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()

	if err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			ws := exitError.Sys().(syscall.WaitStatus)
			exitcode = ws.ExitStatus()
		} else {
			exitcode = 255
		}
	} else {
		ws := cmd.ProcessState.Sys().(syscall.WaitStatus)
		exitcode = ws.ExitStatus()
	}

	return stdout, stderr, exitcode
}

func runCommandAsUserWithEnv(env []string, username string, group string, command string, arg ...string) error {
	cmd := exec.Command(command, arg...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if env != nil {
		env = append(env, os.Environ()...)
		cmd.Env = env
	}
	cmd.SysProcAttr = &syscall.SysProcAttr{}

	_uid, err := user.Lookup(username)
	if err != nil {
		logplus.Error(err.Error())
	}
	if group != "" {

	}
	uid, _ := strconv.ParseUint(_uid.Uid, 10, 32)
	if group != "" {
		_gid, err := user.LookupGroup(group)
		if err != nil {
			logplus.Error(err.Error())
		}

		gid, _ := strconv.ParseUint(_gid.Gid, 10, 32)
		cmd.SysProcAttr.Credential = &syscall.Credential{Uid: uint32(uid), Gid: uint32(gid)}
	} else {
		gid, _ := strconv.ParseUint(_uid.Gid, 10, 32)
		cmd.SysProcAttr.Credential = &syscall.Credential{Uid: uint32(uid), Gid: uint32(gid)}
	}

	err = cmd.Run()
	return err
}
