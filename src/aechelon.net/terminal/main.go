package main

// TODO: rename all runCommand output variables to stdout

import (
	"fmt"
	"github.com/docker/docker/client"
	"golang.org/x/net/context"
	"os"
	"time"

	"aechelon.net/logplus"
	"gopkg.in/urfave/cli.v2"
)

const (
	name string = "Terminal"
	version string = "0.0.3"
)

var (
	flags   CliFlags
	logger* logplus.Logger
	env		Environment
	docker*	client.Client
	ctx context.Context
)

func printIntro() {
	fmt.Println("=======================================")
	//fmt.Printf("%s v%s\n\t- using Docker v%s\n\t- using %s\n", name, version, env.Docker.Version, runtime.Version())
	fmt.Println("=======================================\n")
}

func main() {
	logger = logplus.New()
	ctx = context.Background()
	if c, err := client.NewClientWithOpts(); err != nil {
		logger.Error("Docker was not found")
	} else {
		docker = c
	}
	env.Docker.Version = docker.ClientVersion()

	app := cli.App{
		Name:     name,
		Version:  version,
		Usage: "A Docker project management tool",
		Compiled: time.Now(),
		Authors: []*cli.Author{
			{
				Name:  "Tobias Bohrmann",
				Email: "drawback.grimestat+gitlab@aechelon.net",
			},
		},
		Commands: []*cli.Command{
			{
				Name:    "release",
				Aliases: []string{"r"},
				Usage:   "Release shit",
				//UsageText: "HHH",
				//Description: "HOHOHO",
				//ArgsUsage: "HHHHsssss",
				Action: release,
			},
			{
				Name:    "clean",
				Aliases: []string{"c"},
				Usage:   "Remove orphaned containers and images",
				//UsageText: "HHH",
				//Description: "HOHOHO",
				//ArgsUsage: "HHHHsssss",
				Action: clean,
			},
		},
		Flags: []cli.Flag{
			/*&cli.BoolFlag{
				Name:        "debug",
				Usage:       "Print all program execution output",
				Destination: &flags.debug,
			},*/
		},
		Before: func(c *cli.Context) error {
			return nil
		},
		After: func(c *cli.Context) error {
			return nil
		},
		Action: action,
		CommandNotFound: func(c *cli.Context, command string) {
			fmt.Fprintf(c.App.Writer, "Error: Command %q not found\n", command)
		},
	}
	app.Run(os.Args)
}

func action(c *cli.Context) error {
	if c.Args().Present() {
		if isFile(c.Args().First()) {
			println("YAY")
		}
	}
	return nil
}

func release(c *cli.Context) error {
	return nil
}

func clean(c *cli.Context) error {
	//removeDeadContainers()
	//removeOrphanedImages()
	//removeOrphanedVolumes()

	return nil
}