package main

import (
	"bytes"
	"strings"
)

func cleanOutput(buffer bytes.Buffer) string {
	return strings.TrimRight(buffer.String(), "\n")
}

func getFirstLine(buffer bytes.Buffer) string {
	lines := strings.Split(buffer.String(), "\n")

	return lines[0]
}
