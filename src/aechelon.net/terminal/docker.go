package main

import (
	"aechelon.net/logplus"
	"bufio"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path"
	"regexp"
	"strings"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"golang.org/x/net/context"
)

const (
	DockerVersionPattern = "([0-9]{1,2}.[0-9]{1,2}.[0-9]{1,2})(-ce)?"
	DockerContainerPattern = "[a-z0-9]{64}"
	DockerShortContainerIDPattern = "[a-f0-9]{12}"
)

func getDockerPath() string {
	path, err := exec.LookPath("docker")

	if err != nil {
		logplus.Error("Docker not found")
	}

	return path
}

func getDockerVersion(path string) string {
	args := []string{"--version"}

	if stdout, _, exitcode := runCommandWithOutput(path, args...); exitcode == 255 {
		logplus.Errorf("getDockerVersion(): Cannot execute 'docker %s'", args)
	} else {
		re := regexp.MustCompile(DockerVersionPattern)
		matches := re.FindStringSubmatch(stdout.String())

		if len(matches) == 2 && matches[1] == "" {
			logplus.Error("Can not determine Docker version")
		}

		return matches[1]
	}
}

func doesContainerExist(name string) bool {
	args := []string{"ps", "-a", "-f", fmt.Sprintf("name=%s", name), "-q"}

	if stdout, stderr, exitcode := runCommandWithOutput(env.Docker.Path, args...); exitcode == 255 {
		logger.Errorf("doesContainerExist(): Cannot execute '%s'", args)
	} else {
		if exitcode == 125 {
			logplus.Errorf("doesContainerExist(): '%s' returned with an error: %s", args, stderr)
		}

		if match, _ := regexp.Match(DockerShortContainerIDPattern, stdout.Bytes()); match == true {
			return true
		}
	}

	return false
}

func createDockerNetwork(network Network) {
	cmdArgs := []string{"network", "inspect", network.Name}
	if stdout, stderr, exitcode, err := runCommandWithOutput(env.Docker.Path, cmdArgs...); err != nil {
		logger.Error(err.Error())
	} else {
		if exitcode != 0 && stdout != "[]" && stderr == "" {
			if stdout == "[]" {
				cmdArgs := []string{"network", "create", network}
				if _, stderr, err := runCommand(env.Docker.Path, cmdArgs...); err != nil {
					logger.Error(errors.New(stderr))
				} else {
					logger.Printfln("Network '%s' created", network)
				}
			} else {
				logger.Printfln("Network '%s' already exists", network)
			}
		}
	}
}

func cleanDockerMounts(dataDirectory string) {
	var userChoice bool

	fi, err := os.Stat(dataDirectory)
	if !os.IsNotExist(err) {
		switch mode := fi.Mode(); {
		case mode.IsDir():
			if flags.purge == true {
				if flags.force == false {
					userChoice = askForConfirmation(fmt.Sprintf("Are you sure you want to delete all contents of directory '%s'? [yes/no] : ", dataDirectory))
				} else {
					userChoice = true
				}
				if userChoice == true {
					logger.Printfln("Clean directory '%s'", dataDirectory)
					removeDirectory(dataDirectory)
				}
			}
		case mode.IsRegular():
			logger.Warningf("'%s' points to file", dataDirectory)
		}
	}
}

func killDockerContainer(name string) {
	var userChoice bool

	if flags.force == false {
		userChoice = askForConfirmation(fmt.Sprintf("Are you sure you want to remove container '%s'? [yes/no] : ", name))
	} else {
		userChoice = true
	}
	if userChoice == true {
		cmdArgs := []string{"stop", name}
		if _, _, err := runCommand(env.Docker.Path, cmdArgs...); err != nil {
			logger.Error(err)
		}
		logger.Println("Container stopped")
		cmdArgs = []string{"rm", "-f", name}
		if stdout, stderr, err := runCommand(env.Docker.Path, cmdArgs...); err != nil {
			logger.Error(errors.New(stderr))
		} else {
			if len(stdout) > 0 {
				logger.Printfln("Container '%s' removed", name)
			} else {
				logger.Warningf("Container '%s' does not exist", name)
			}
		}
	} else {
		logger.Println("Container was spared")
	}
}

func removeDeadContainers() {
	var container []string
	cmdArgsCreated := []string{"ps", "-q", "-f", "status=created"}
	cmdArgsExited := []string{"ps", "-q", "-f", "status=exited"}
	if stdout, stderr, err := runCommand(env.Docker.Path, cmdArgsCreated...); err != nil {
		logger.Error(errors.New(stderr))
	} else {
		s := bufio.NewScanner(strings.NewReader(stdout))
		for s.Scan() {
			container = append(container, s.Text())
		}
	}
	if stdout, stderr, err := runCommand(env.Docker.Path, cmdArgsExited...); err != nil {
		logger.Error(errors.New(stderr))
	} else {
		s := bufio.NewScanner(strings.NewReader(stdout))
		for s.Scan() {
			container = append(container, s.Text())
		}
	}
	if len(container) > 0 {
		logger.Printfln("=> Remove %v dead containers", len(container))
		args := []string{"rm"}
		if _, stderr, err := runCommand(env.Docker.Path, append(args, container...)...); err != nil {
			logger.Error(errors.New(stderr))
		}
	}
}

func removeOrphanedImages() {
	var images []string
	cmdArgs := []string{"images", "-q", "-f", "dangling=true"}
	if stdout, stderr, err := runCommand(env.Docker.Path, cmdArgs...); err != nil {
		logger.Error(errors.New(stderr))
	} else {
		s := bufio.NewScanner(strings.NewReader(stdout))
		for s.Scan() {
			images = append(images, s.Text())
		}
	}
	if len(images) > 0 {
		logger.Printfln("=> Remove %v orphaned images", len(images))
		args := []string{"rmi"}
		if _, stderr, err := runCommand(env.Docker.Path, append(args, images...)...); err != nil {
			logger.Error(errors.New(stderr))
		}
	}
}

func removeOrphanedVolumes() {
	var volumes []string
	cmdArgs := []string{"volume", "ls", "-q", "-f", "dangling=true"}
	if stdout, stderr, err := runCommand(env.Docker.Path, cmdArgs...); err != nil {
		logger.Error(errors.New(stderr))
	} else {
		s := bufio.NewScanner(strings.NewReader(stdout))
		for s.Scan() {
			volumes = append(volumes, s.Text())
		}
	}
	if len(volumes) > 0 {
		logger.Printfln("=> Remove %v orphaned volumes", len(volumes))
		args := []string{"volume", "rm"}
		if _, stderr, err := runCommand(env.Docker.Path, append(args, volumes...)...); err != nil {
			logger.Error(errors.New(stderr))
		}
	}
}

func removeContainer(project Project) {
	containerName := getContainerName(project.Data.General.Name)
	args := []string{"rm", "-f", containerName}
	stdout, stderr, err := runCommand(env.Docker.Path, args...)
	if err != nil {
		logger.Error(errors.New(stderr))
	}
	if strings.TrimRight(stdout, "\n") != containerName {
		logger.Error(errors.New(fmt.Sprintf("Error removing container '%s'", containerName)))
	}
	logger.Printfln("Container '%s' successfully removed", containerName)
}

func startContainer(project Project) {
	var args = []string{"create"}

	containerName := getContainerName(project.Data.General.Name)

	args = append(args, []string{"--name", containerName}...)
	if project.Data.Docker.RestartPolicy != "" {
		args = append(args, []string{"--restart", project.Data.Docker.RestartPolicy}...)
	}
	if project.Data.Docker.Hostname != "" {
		args = append(args, []string{"--hostname", project.Data.Docker.Hostname}...)
	}
	for _, mount := range project.Data.Docker.Mounts {
		split := strings.Split(mount, ":")
		if len(split) > 1 {
			args = append(args, []string{"--volume", fmt.Sprintf("%s:%s", path.Join(project.Data.Host.DataDirectory, split[0]), mount)}...)
		} else {
			args = append(args, []string{"--volume", fmt.Sprintf("%s:%s", path.Join(project.Data.Host.DataDirectory, mount), mount)}...)
		}
	}
	for _, port := range project.Data.Docker.PrivatePorts {
		args = append(args, []string{"--expose", fmt.Sprintf("%v", port)}...)
	}
	for _, port := range project.Data.Docker.PublicPorts {
		args = append(args, []string{"--publish", fmt.Sprintf("%s", port)}...)
	}
	if project.Data.Docker.AdditionalOptions != "" {
		args = append(args, strings.Split(project.Data.Docker.AdditionalOptions, " ")...)
	}
	if flags.version != "" {
		args = append(args, fmt.Sprintf("%s:%s", project.Data.General.Name, flags.version))
	} else {
		args = append(args, fmt.Sprintf("%s:%s", project.Data.General.Name, project.Data.General.Version))
	}

	stdout, stderr, err := runCommand(env.Docker.Path, args...)
	if err != nil {
		logger.Error(errors.New(stderr))
	}
	re := regexp.MustCompile(DockerContainerPattern)
	if re.MatchString(stdout) == false {
		logger.Error(errors.New(fmt.Sprintf("Error creating container '%s'", project.Data.General.Name)))
	}

	args = []string{"network", "disconnect", "bridge", containerName}
	stdout, stderr, err = runCommand(env.Docker.Path, args...)
	if err != nil {
		logger.Error(errors.New("Cannot disconnect container from network bridge"))
	}

	for _, network := range project.Data.Docker.Networks {
		args = []string{"network", "connect", network, containerName}
		_, stderr, err := runCommand(env.Docker.Path, args...)
		if err != nil {
			logger.Warning(stderr)
		}
	}

	args = []string{"start", containerName}
	stdout, stderr, err = runCommand(env.Docker.Path, args...)
	if err != nil {
		logger.Error(errors.New(stderr))
	}
	if strings.TrimRight(stdout, "\n") != containerName {
		logger.Error(errors.New(fmt.Sprintf("Error starting container '%s'", containerName)))
	}
	logger.Printfln("Container '%s' successfully started", containerName)
}

func printDockerLog(container string) {
	args := []string{"logs", "-f", container}
	cmd := exec.Command(env.Docker.Path, args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Run()
}

func execIntoContainer(container string, command string) {
	args := []string{"exec", "-it", container, command}
	cmd := exec.Command(env.Docker.Path, args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	cmd.Run()
}
*/