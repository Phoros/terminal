package pwdgen

import (
	"crypto/rand"
	"io"
)

const (
	Weak = iota
	NoSpecial
	Strong
)

var characterMap = [][]byte{
	[]byte("abcdefghijklmnopqrstuvwxyz0123456789"),                                                     // Weak
	[]byte("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"),                           // No special character
	[]byte("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()-_=+,.?/:;{}[]`~"), // Strong
}

func Password(length int, quality int) string {
	return rand_char(length, quality)
}

func rand_char(length int, quality int) string {
	new_pword := make([]byte, length)
	random_data := make([]byte, length+(length/4)) // storage for random bytes.
	chars := characterMap[quality]
	clen := byte(len(chars))
	maxrb := byte(256 - (256 % len(chars)))
	i := 0
	for {
		if _, err := io.ReadFull(rand.Reader, random_data); err != nil {
			panic(err)
		}
		for _, c := range random_data {
			if c >= maxrb {
				continue
			}
			new_pword[i] = chars[c%clen]
			i++
			if i == length {
				return string(new_pword)
			}
		}
	}
	panic("unreachable")
}
