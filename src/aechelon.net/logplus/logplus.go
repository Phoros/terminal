package logplus

import (
	"fmt"
	"log"
	"os"

	"github.com/fatih/color"
)

type Logger struct {
	path string
	file *os.File
	log  log.Logger
}

func New() *Logger {
	l := &Logger{}
	return l
}

func NewFileLogger(mode int, path string) *Logger {
	fl := &Logger{}
	fl.path = path
	f, err := os.OpenFile(fl.path, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0755)
	if err != nil {
		panic(fmt.Sprintf("Can not open / create file '%s' for logging", fl.path))
	} else {
		fl.file = f
	}

	return fl
}

func (l *Logger) Print(s string) {
	if l.file != nil {
		l.log.Print(s)
	}
}

func Print(s string) {
	fmt.Print(s)
}

func (l *Logger) Printf(format string, a ...interface{}) {
	if l.file != nil {
		l.log.Printf(format, a...)
	}
}

func Printf(format string, a ...interface{}) {
	fmt.Printf(format, a...)
}

func (l *Logger) Println(s string) {
	if l.file != nil {
		l.log.Println(s)
	}
}

func Println(s string) {
	fmt.Println(s)
}

func (l *Logger) Printfln(format string, a ...interface{}) {
	if l.file != nil {
		l.log.Printf(format+"\n", a...)
	}
}

func Printfln(format string, a ...interface{}) {
	fmt.Printf(format+"\n", a...)
}

func (l *Logger) Warning(s string) {
	if l.file != nil {
		l.log.Printf("WARNING: %s", s)
	}
}

func Warning(s string) {
	fmt.Print(color.YellowString(s))
}

func (l *Logger) Warningf(format string, a ...interface{}) {
	if l.file != nil {
		l.log.Printf("WARNING: "+format+"\n", a...)
	}
}

func Warningf(format string, a ...interface{}) {
	fmt.Printf(color.YellowString(format), a...)
}

func (l *Logger) Warningln(s string) {
	if l.file != nil {
		l.log.Printf("WARNING: %s", s)
	}
}

func Warningln(s string) {
	fmt.Println(color.YellowString(s))
}

func (l *Logger) Warningfln(format string, a ...interface{}) {
	if l.file != nil {
		l.log.Printf("WARNING: "+format+"\n", a...)
	}
}

func Warningfln(format string, a ...interface{}) {
	fmt.Printf(color.YellowString(format+"\n"), a...)
}

func (l *Logger) Error(s string) {
	if l.file != nil {
		l.log.Printf("ERROR: %s", s)
	}
	os.Exit(1)
}

func Error(s string) {
	fmt.Print(color.RedString(s))
	os.Exit(1)
}

func (l *Logger) Errorf(format string, a ...interface{}) {
	if l.file != nil {
		l.log.Printf("ERROR: "+format, a...)
	}
	os.Exit(1)
}

func Errorf(format string, a ...interface{}) {
	fmt.Printf(color.RedString(format), a...)
	os.Exit(1)
}

func (l *Logger) Errorln(s string) {
	if l.file != nil {
		l.log.Println("ERROR: "+s+"\n")
	}
	os.Exit(1)
}

func Errorln(s string) {
	fmt.Println(color.RedString(s))
	os.Exit(1)
}

func (l *Logger) Errorfln(format string, a ...interface{}) {
	if l.file != nil {
		l.log.Printf("ERROR: "+format+"\n", a...)
	}
	os.Exit(1)
}

func Errorfln(format string, a ...interface{}) {
	fmt.Printf(color.RedString("format"+"\n"), a...)
	os.Exit(1)
}

func (l *Logger) Close() {
	if l.file != nil {
		l.file.Close()
	}
	l = &Logger{}
}
